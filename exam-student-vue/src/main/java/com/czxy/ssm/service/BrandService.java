package com.czxy.ssm.service;

import com.czxy.ssm.domain.Brand;

import java.util.List;

/**
 * @author ChinaManor
 * #Description BrandService
 * #Date: 26/10/2021 21:48
 */
public interface BrandService {

    /**
     * 查询所有
     *
     * @return
     */
    public List<Brand> selectAll();
}

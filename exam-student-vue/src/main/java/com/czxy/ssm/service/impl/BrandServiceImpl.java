package com.czxy.ssm.service.impl;

import com.czxy.ssm.domain.Brand;
import com.czxy.ssm.mapper.BrandMapper;
import com.czxy.ssm.service.BrandService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class BrandServiceImpl implements BrandService {

    @Resource
    private BrandMapper brandMapper;


    /**
     * 查询所有
     *
     * @return
     */

    @Override
    public List<Brand> selectAll() {
        List<Brand> List = brandMapper.selectAll();
        return List;
    }

}

package com.czxy.ssm.service;

import com.czxy.ssm.domain.Goods;
import com.czxy.ssm.vo.goodsVo;
import com.github.pagehelper.PageInfo;

public interface GoodsService {

    PageInfo<Goods> condition(Integer pageSize, Integer pageNum, goodsVo goodsVo);
    public Goods findById(String studentId);
    public boolean update(Goods goods);
}

package com.czxy.ssm.service.impl;

import com.czxy.ssm.domain.Category;
import com.czxy.ssm.mapper.CategoryMapper;
import com.czxy.ssm.mapper.GoodsMapper;
import com.czxy.ssm.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    @Resource
    private CategoryMapper categoryMapper;

    @Resource
    private GoodsMapper goodsMapper;

    /**
     * 查询所有
     *
     * @return
     */

    @Override
    public List<Category> selectAll() {
        List<Category> List = categoryMapper.selectAll();
        return List;
    }

}

package com.czxy.ssm.service;

import com.czxy.ssm.domain.Category;

import java.util.List;

public interface CategoryService {
    /**
     * 查询所有
     *
     * @return
     */
    public List<Category> selectAll();


}

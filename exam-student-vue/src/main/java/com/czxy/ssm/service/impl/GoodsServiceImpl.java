package com.czxy.ssm.service.impl;

import com.czxy.ssm.domain.Brand;
import com.czxy.ssm.domain.Category;
import com.czxy.ssm.domain.Goods;
import com.czxy.ssm.mapper.BrandMapper;
import com.czxy.ssm.mapper.CategoryMapper;
import com.czxy.ssm.mapper.GoodsMapper;
import com.czxy.ssm.service.GoodsService;
import com.czxy.ssm.vo.goodsVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class GoodsServiceImpl implements GoodsService {
    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private CategoryMapper categoryMapper;
    //设置dao层的数据传输
    @Resource
    private BrandMapper brandMapper;

//    TODO 查询所有
    @Override
    public PageInfo<Goods> condition(Integer pageSize, Integer pageNum, goodsVo goodsVo) {
        // 1 拼凑条件
        Example example = new Example(Goods.class);
        Example.Criteria criteria = example.createCriteria();
        if (goodsVo.getCategory_id() != null && !"".equals(goodsVo.getCategory_id())) {
            criteria.andEqualTo("category_id", goodsVo.getCategory_id());
        }
        if (goodsVo.getBrand_id() != null && !"".equals(goodsVo.getBrand_id())) {
            criteria.andEqualTo("brand_id", goodsVo.getBrand_id());
        }
        if (goodsVo.getName() != null && !"".equals(goodsVo.getName())) {
            criteria.andLike("name", "%" + goodsVo.getName() + "%");
        }
        if (goodsVo.getStartAge() != null && !"".equals(goodsVo.getStartAge())) {
            criteria.andGreaterThanOrEqualTo("price", goodsVo.getStartAge());
        }
        if (goodsVo.getEndAge() != null && !"".equals(goodsVo.getEndAge())) {
            criteria.andLessThanOrEqualTo("price", goodsVo.getEndAge());
        }

        // 2 分页查询
        PageHelper.startPage(pageNum, pageSize);

        // 3 查询
        List<Goods> list = goodsMapper.selectByExample(example);

        // 4 处理管理数据
        for (Goods goods : list) {
            Category category = categoryMapper.selectByPrimaryKey(goods.getCategory_id());
            goods.setCategory(category);
        }
        for (Goods goods : list) {
            Brand brand = brandMapper.selectByPrimaryKey(goods.getBrand_id());
            goods.setBrand(brand);
        }
        // 5 返回
        return new PageInfo<>(list);
    }

    /**
     * 查询详情
     * @param studentId
     * @return
     */
    @Override
    public Goods findById(String studentId){
        Goods goods = goodsMapper.selectByPrimaryKey(studentId);
        return goods;
    }

    /**
     * 更新操作
     * @param goods
     * @return
     */
    @Override
    public boolean update(Goods goods){
        // 校验
        Goods find = goodsMapper.selectByPrimaryKey(goods.getId());
        if(find == null) {
            throw new RuntimeException("ID不存在");
        }
        // 更新非空项
        int count = goodsMapper.updateByPrimaryKeySelective(goods);
        return count == 1;
    }
}

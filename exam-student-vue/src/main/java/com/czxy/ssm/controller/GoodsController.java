package com.czxy.ssm.controller;

import com.czxy.ssm.domain.Goods;
import com.czxy.ssm.service.GoodsService;
import com.czxy.ssm.vo.BaseResult;
import com.czxy.ssm.vo.goodsVo;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/goods")
@CrossOrigin
public class GoodsController {
    @Resource
    private GoodsService goodsService;

    /**
     * http://localhost:8080/student/condition/3/1
     *
     * @return
     */

    @PostMapping("/condition/{pageSize}/{pageNum}")
    public BaseResult condition(
            @PathVariable("pageSize") Integer pageSize,
            @PathVariable("pageNum") Integer pageNum,
            @RequestBody goodsVo goodsVo) {
        //查询
        PageInfo<Goods> pageInfo = goodsService.condition(pageSize, pageNum, goodsVo);
        //返回结果
        return BaseResult.ok("查询成功", pageInfo);

    }

    /**
     * 查询详情
     * @param studentId
     * @return
     */
    @GetMapping("/{id}")
    public BaseResult findById(@PathVariable("id") String studentId ){
        // 查询
        Goods goods = goodsService.findById(studentId);
        if(goods != null){
            // 返回
            return BaseResult.ok("查询成功", goods);
        } else {
            return BaseResult.error("查询失败");
        }
    }

    /**
     * 更新操作
     * @param goods
     * @return
     */
    @PutMapping
    public BaseResult update(@RequestBody Goods goods){
        try {
            // 添加
            boolean result = goodsService.update(goods);
            // 返回
            if(result){
                return BaseResult.ok("更新成功");
            } else {
                return BaseResult.error("更新失败");
            }
        } catch (Exception e) {
            return BaseResult.error(e.getMessage());
        }
    }
}

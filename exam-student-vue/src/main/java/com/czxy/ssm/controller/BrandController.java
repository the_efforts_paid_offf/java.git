package com.czxy.ssm.controller;

import com.czxy.ssm.domain.Brand;
import com.czxy.ssm.service.BrandService;
import com.czxy.ssm.vo.BaseResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/brand")
@CrossOrigin
public class BrandController {

    @Resource
    private BrandService brandService;

    /**
     * 查询所有
     *
     * @return
     */

    @GetMapping
    public BaseResult<List<Brand>> list() {
        List<Brand> List = brandService.selectAll();
        return BaseResult.ok("查询成功", List);
    }
}

package com.czxy.ssm.mapper;


import com.czxy.ssm.domain.Goods;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface GoodsMapper extends Mapper<Goods> {
}

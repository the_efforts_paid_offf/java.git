package com.czxy.ssm.mapper;

import com.czxy.ssm.domain.Category;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface CategoryMapper extends Mapper<Category> {
}

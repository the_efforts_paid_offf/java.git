package com.czxy.ssm.mapper;

import com.czxy.ssm.domain.Brand;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ChinaManor
 * #Description BrandMapper
 * #Date: 26/10/2021 21:50
 */
@org.apache.ibatis.annotations.Mapper
public interface BrandMapper extends Mapper<Brand> {
}

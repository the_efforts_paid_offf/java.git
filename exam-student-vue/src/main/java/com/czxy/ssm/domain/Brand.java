package com.czxy.ssm.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ChinaManor
 * #Description Brand
 * #Date: 26/10/2021 21:44
 */
@Table(name = "brand")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Brand {
    //    id
//            name
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "name")
    private String name;

}

package com.czxy.ssm.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


@Table(name = "category")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    //    id
//            name
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "name")
    private String name;

}

package com.czxy.ssm.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "goods")
public class Goods {

    //id
    //name
    //brand_id
    //category_id
    //introduction
    //store_num
    //price
    //create_time
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "brand_id")
    private Integer brand_id;

    @Column(name = "category_id")
    private Integer category_id;

    @Column(name = "introduction")
    private String introduction;

    @Column(name = "store_num")
    private String store_num;

    @Column(name = "price")
    private String price;
    @Column(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date create_time;

    // 引入其他的类
    private Category category;
    private Brand brand;


}

package com.czxy.vo;

import lombok.Data;

@Data
public class TeacherSearchVO {

    private String teacherName;
    private Integer schoolId;
    private Integer minSalary;
    private Integer maxSalary;
    private String minDate;
    private String maxDate;
    private Integer page;//页码
    private Integer rows;//行数





}

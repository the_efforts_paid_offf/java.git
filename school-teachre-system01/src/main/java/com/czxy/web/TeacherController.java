package com.czxy.web;

import com.czxy.common.BaseResult;
import com.czxy.entity.TTeacher;
import com.czxy.service.impl.TeacherServiceImpl;
import com.czxy.vo.TeacherSearchVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    private TeacherServiceImpl teacherService;

    @GetMapping("/condition")
    public ResponseEntity<BaseResult> findByCondition(TeacherSearchVO teacherSearchVO){
        ResponseEntity<BaseResult> rbr = teacherService.findByCondition(teacherSearchVO);
        return rbr;
    }

    @PostMapping
    public ResponseEntity<BaseResult> add(@RequestBody TTeacher tea){
        boolean flag = teacherService.add(tea);
        if (flag){
            return BaseResult.success("新增成功");
        }else{
            return BaseResult.fail();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResult> findById(@PathVariable("id") Integer id){
        TTeacher tea = teacherService.findById(id);
        return BaseResult.success(tea);
    }


    @PutMapping
    public ResponseEntity<BaseResult> update(@RequestBody TTeacher tea){
        boolean flag = teacherService.update(tea);
        if (flag){
            return BaseResult.success("修改成功");
        }else{
            return BaseResult.fail();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResult> deleteById(@PathVariable Integer id){
        boolean flag= teacherService.deleteById(id);
        if (flag){
            return BaseResult.success("删除成功");
        }else{
            return BaseResult.fail();
        }
    }
    @DeleteMapping("/batchDelete/{ids}")
    public ResponseEntity<BaseResult> bachDelete(@PathVariable String ids){
        String[] splitIds = ids.split(",");
        for (String id:splitIds){
            teacherService.deleteById(Integer.parseInt(id));
        }
        return BaseResult.success("删除成功");
    }

}




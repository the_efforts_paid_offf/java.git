package com.czxy.web;

import com.czxy.common.BaseResult;
import com.czxy.entity.TType;
import com.czxy.service.impl.TypeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/type")
public class TypeController {

    @Autowired
    private TypeServiceImpl typeService;

    @GetMapping("/all")
    public ResponseEntity<BaseResult> findAll(){
        List<TType> list = typeService.findAll();
        return BaseResult.success(list);
    }






}

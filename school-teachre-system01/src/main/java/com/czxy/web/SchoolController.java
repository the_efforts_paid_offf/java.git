package com.czxy.web;

import com.czxy.common.BaseResult;
import com.czxy.entity.TSchool;
import com.czxy.service.impl.SchoolServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/school")
public class SchoolController {

    @Autowired
    private SchoolServiceImpl schoolService;

    @GetMapping("/all")
    public ResponseEntity<BaseResult> findAll(){
        List<TSchool> all = schoolService.findAll();
        return BaseResult.success(all);
    }

}

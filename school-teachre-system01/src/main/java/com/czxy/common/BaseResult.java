package com.czxy.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.ResponseEntity;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseResult {

    /**
     * 10000 成功
     * 10001 失败
     */
    private Integer code;
    /**
     * 成功或者失败的消息提醒
     */
    private String msg;
    /**
     * 数据
     */
    private Object data;

    private Integer total;

    public BaseResult(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static ResponseEntity<BaseResult> success(Object data){
        return ResponseEntity.ok(new BaseResult(10000,"成功",data));
    }

    public static ResponseEntity<BaseResult> fail(){
        return ResponseEntity.ok(new BaseResult(10001,"失败",null));
    }

    public static ResponseEntity<BaseResult> success(Object data,Integer total){
        return ResponseEntity.ok(new BaseResult(10000,"成功",data,total));
    }

}

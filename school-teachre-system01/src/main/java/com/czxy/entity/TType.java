package com.czxy.entity;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "t_type")
//@Getter
//@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class TType implements Serializable {

    @Id
	private Integer id;
	private String city;

}

package com.czxy.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "t_school")
@Data//自动生成getter和setter方法
@NoArgsConstructor//空参构造
@AllArgsConstructor//全参构造
public class TSchool implements Serializable {

    @Id
	private Integer id;
	private String schoolName;

}

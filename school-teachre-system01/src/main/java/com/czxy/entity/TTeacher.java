package com.czxy.entity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "t_teacher")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TTeacher implements Serializable {

    @Id
	private Integer id;
	private String teacherName;
	private String pwd;
	private String sex;
	private BigDecimal salary;
	private Integer typeId;
	private Integer schoolId;
//	@Column(name = "`hire_date`")
//	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	// 在java的日期类型只要符合2012-02-02
	private String hireDate;
	private String remark;

	private TSchool school;
	private TType type;

}

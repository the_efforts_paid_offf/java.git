package com.czxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolTeachreSystem01Application {

    public static void main(String[] args) {
        SpringApplication.run(SchoolTeachreSystem01Application.class, args);
    }

}

package com.czxy.service;


import com.czxy.common.BaseResult;
import com.czxy.entity.TTeacher;
import com.czxy.vo.TeacherSearchVO;
import org.springframework.http.ResponseEntity;

public interface TeacherService {
    ResponseEntity<BaseResult> findByCondition(TeacherSearchVO teacherSearchVO);

    boolean add(TTeacher tea);

    TTeacher findById(Integer id);

    boolean update(TTeacher tea);

    boolean deleteById(Integer id);
}

package com.czxy.service;


import com.czxy.entity.TSchool;

import java.util.List;

public interface SchoolService {

    List<TSchool> findAll();
}

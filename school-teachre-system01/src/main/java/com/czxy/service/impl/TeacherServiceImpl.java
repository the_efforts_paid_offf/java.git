package com.czxy.service.impl;

import com.czxy.common.BaseResult;
import com.czxy.dao.SchoolMapper;
import com.czxy.dao.TeacherMapper;
import com.czxy.dao.TypeMapper;
import com.czxy.entity.TTeacher;
import com.czxy.service.TeacherService;
import com.czxy.vo.TeacherSearchVO;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherMapper teacherMapper;

    @Autowired
    private SchoolMapper schoolMapper;

    @Autowired
    private TypeMapper typeMapper;

    @Override
    public ResponseEntity<BaseResult> findByCondition(TeacherSearchVO teacherSearchVO) {
       // List<TTeacher> teaList = new ArrayList<>();//最终要返回到前端的数据
        //拼接条件
        String teacherName = teacherSearchVO.getTeacherName();
        Integer schoolId = teacherSearchVO.getSchoolId();
        Integer minSalary = teacherSearchVO.getMinSalary();
        Integer maxSalary = teacherSearchVO.getMaxSalary();
        String minDate = teacherSearchVO.getMinDate();
        String maxDate = teacherSearchVO.getMaxDate();
        Integer page = teacherSearchVO.getPage();
        Integer rows = teacherSearchVO.getRows();

        Example example = new Example(TTeacher.class);
        Example.Criteria criteria = example.createCriteria();
        //1 名字模糊
        if (StringUtils.isNotBlank(teacherName)){
            criteria.andLike("teacherName","%"+teacherName+"%");
        }
        //2 学校id精确匹配
        if (schoolId!=null&&StringUtils.isNotBlank(schoolId.toString())){
            criteria.andEqualTo("schoolId",schoolId);
        }
        //3 >=最低薪资
        if (minSalary!=null&&StringUtils.isNotBlank(minSalary.toString())){
            criteria.andGreaterThanOrEqualTo("salary",minSalary);
        }
        //4 <=最高薪资
        if (maxSalary!=null&&StringUtils.isNotBlank(maxSalary.toString())){
            criteria.andLessThanOrEqualTo("salary",maxSalary);
        }
        //5 >=入职时间
        if (StringUtils.isNotBlank(minDate)){
            criteria.andGreaterThanOrEqualTo("hireDate",minDate);
        }
        //6 <=入职时间
        if (StringUtils.isNotBlank(maxDate)){
            criteria.andLessThanOrEqualTo("hireDate",maxDate);
        }
        //查找总条数，供分页使用
        int total = teacherMapper.selectByExample(example).size();

        //7 后端分页
        PageHelper.startPage(page,rows);

        //8 查出结果
        List<TTeacher> teacherList = teacherMapper.selectByExample(example);

        //后端关联id显示名字
        for (TTeacher tea:teacherList){
            tea.setType(typeMapper.selectByPrimaryKey(tea.getTypeId()));
            tea.setSchool(schoolMapper.selectByPrimaryKey(tea.getSchoolId()));
        }

        if (teacherList.size()==0&&teacherSearchVO.getPage()!=1){
            page = total%rows==0?total/rows:(total/rows+1);
            teacherSearchVO.setPage(page);
            findByCondition(teacherSearchVO);
        }

        return BaseResult.success(teacherList,total);
    }

    @Override
    public boolean add(TTeacher tea) {
        return teacherMapper.insert(tea)==1;
    }

    @Override
    public TTeacher findById(Integer id) {
        return teacherMapper.selectByPrimaryKey(id);
    }

    @Override
    public boolean update(TTeacher tea) {
        return teacherMapper.updateByPrimaryKey(tea)==1;
    }


    @Override
    public boolean deleteById(Integer id) {
        return teacherMapper.deleteByPrimaryKey(id)==1;
    }
}

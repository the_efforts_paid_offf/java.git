package com.czxy.service.impl;

import com.czxy.dao.SchoolMapper;
import com.czxy.entity.TSchool;
import com.czxy.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SchoolServiceImpl implements SchoolService {

    @Autowired
    private SchoolMapper schoolMapper;

    @Override
    public List<TSchool> findAll() {
        return schoolMapper.selectAll();
    }
}

package com.czxy.service;


import com.czxy.entity.TType;

import java.util.List;

public interface TypeService {

    List<TType> findAll();

}

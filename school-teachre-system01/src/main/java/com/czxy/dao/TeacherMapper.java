package com.czxy.dao;

import com.czxy.entity.TTeacher;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface TeacherMapper extends Mapper<TTeacher> {

}

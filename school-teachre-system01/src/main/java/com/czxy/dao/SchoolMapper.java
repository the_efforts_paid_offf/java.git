package com.czxy.dao;


import com.czxy.entity.TSchool;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface SchoolMapper extends Mapper<TSchool> {
}

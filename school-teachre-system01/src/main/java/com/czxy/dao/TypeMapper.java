package com.czxy.dao;

import com.czxy.entity.TType;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface TypeMapper extends Mapper<TType> {
}

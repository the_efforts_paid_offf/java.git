package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ChinaManor
 * #Description Product
 * #Date: 21/11/2021 15:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_product")
public class Product {


    //productID
    //product_name
    //product_price
    //product_time
    //brandID
    //categoryID
    @Id
    @Column(name = "productID")
    private Integer productID;
    @Column(name = "product_name")
    private String product_name;
    @Column(name = "product_price")
    private Double product_price;
    @Column(name = "product_time")
    private String product_time;
    @Column(name = "brandID")
    private Integer brandID;
    @Column(name = "categoryID")
    private Integer categoryID;
    @Column(name = "createID")
    private Integer createID;

    Brand brand;
    Category category;

}

package czxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserService01Application {

    public static void main(String[] args) {
        SpringApplication.run(UserService01Application.class, args);
    }

}

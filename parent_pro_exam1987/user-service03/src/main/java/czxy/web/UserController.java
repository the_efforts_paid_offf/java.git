package czxy.web;


import czxy.common.BaseResult;
import czxy.dao.ProductSearchVO;
import czxy.service.impl.ProductServiceimpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pojo.Product;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/product")
//@CrossOrigin
public class UserController {
    @Resource
    private ProductServiceimpl productServiceimpl;
    //    TODO 员工的增删改查
/*
    @GetMapping("/allEmp")
    public List<Product> condition(){
        return productServiceimpl.selectAll();
    }
*/


    /*@GetMapping("/condition")
    public ResponseEntity<BaseResult> condition(ProductSearchVO productSearchVO){
        System.out.println(productSearchVO);
        ResponseEntity<BaseResult> rbr = productServiceimpl.selectAll(productSearchVO);
        System.out.println(rbr);
        return rbr;
    }*/
    @PostMapping("/addEmp")
    public BaseResult<List<Product>> addEmp(@RequestBody Product product){
        boolean add = productServiceimpl.add(product);
        System.out.println(add);
        if (add){
            return BaseResult.ok("新增成功！");
        }
        return BaseResult.error("新增失败！");
    }
    @GetMapping("/{id}")
    public BaseResult findById(@PathVariable("id") Integer id){
        Product product = productServiceimpl.findById(id);
        System.out.println(id);
        System.out.println(product);
        return BaseResult.ok("查询成功!", product);
    }

    @PutMapping
    public BaseResult updateByid(@RequestBody Product product){
        boolean flag = productServiceimpl.updateByid(product);
        System.out.println(flag);
        if (flag){
            return BaseResult.ok("修改成功！");
        }
        else {
            return BaseResult.error("修改失败！");
        }
    }

    @DeleteMapping("/{id}")
    public BaseResult deleteById(@PathVariable Integer id){
        boolean flag = productServiceimpl.del(id);
        if (flag){
            return BaseResult.ok("删除成功！");
        }
        else {
            return BaseResult.error("删除失败！");
        }
    }


    @DeleteMapping("/batchDelete/{ids}")
    public ResponseEntity<BaseResult> bachDelete(@PathVariable String ids){
        String[] splitIds = ids.split(",");
        for (String id:splitIds){
            productServiceimpl.deleteById(Integer.parseInt(id));
        }
        return BaseResult.success("删除成功");
    }

}

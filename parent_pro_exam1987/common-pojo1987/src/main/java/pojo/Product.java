package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ChinaManor
 * #Description Product
 * #Date: 21/11/2021 15:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_news")
public class Product {


    //id
    //news_type_id
    //title
    //pub_date
    //mark
    //origin_url
    //content
    //uid
    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "news_type_id")
    private Integer news_type_id;
    @Column(name = "title")
    private String title;
    @Column(name = "pub_date")
    private String pub_date;
    @Column(name = "mark")
    private String mark;
    @Column(name = "origin_url")
    private String origin_url;
    @Column(name = "content")
    private String content;
    @Column(name = "uid")
    private Integer uid;
/*    @Column(name = "createID")
    private Integer createID;*/

    Brand brand;
//    Category category;

}

package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ChinaManor
 * #Description Brand
 * #Date: 19/11/2021 10:47
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_news_type")
public class Brand {
    //id
    //name
    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
}

package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ChinaManor
 * #Description Brand
 * #Date: 19/11/2021 10:47
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_category")
public class Category {
    //categoryID
    //category_name
    @Id
    @Column(name = "categoryID")
    private Integer categoryID;
    @Column(name = "category_name")
    private String category_name;


}

package categoryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication(scanBasePackages = {"czxy.dao"})
public class CategoryserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CategoryserviceApplication.class, args);
    }

}

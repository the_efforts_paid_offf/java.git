package czxy.web;

import czxy.common.BaseResult;
import czxy.service.impl.BrandServiceimpl;
import czxy.service.impl.CategoryServiceimpl;
import czxy.service.impl.ProductServiceimpl;
import czxy.service.impl.UserServiceimpl;
import czxy.util.JWTUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pojo.Brand;
import pojo.Category;
import pojo.Product;
import czxy.dao.ProductSearchVO;
import pojo.User;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/auth")
//@CrossOrigin
public class LoginController {

    @Resource
    private UserServiceimpl userServiceimpl;

    @Resource
    private CategoryServiceimpl categoryServiceimpl;
    @Resource
    private ProductServiceimpl productServiceimpl;


//    @Autowired
//    private HttpServletRequest httpServletRequest;

/*    //TODO 品牌信息
    @GetMapping("/blist")
    public List<Brand> selectAllB(){
        List<Brand> brands = brandServiceimpl.selectAll();
        return brands;
    }



    @PostMapping("/add")
    public BaseResult<List<Brand>> add(@RequestBody Brand brand){
        boolean add = brandServiceimpl.add(brand);
        System.out.println(add);
        if (add){
            return BaseResult.ok("新增成功！");
        }
        return BaseResult.error("新增失败！");
    }*/

/*
    @GetMapping("/condition")
    public List<Product>  selectAlll(){
        List<Product> products = productServiceimpl.selectAll();
        return products;
    }*/

    @GetMapping("/condition")
    public ResponseEntity<BaseResult>  condition(ProductSearchVO productSearchVO){
        System.out.println(productSearchVO);
        ResponseEntity<BaseResult> rbr = productServiceimpl.selectAll(productSearchVO);
        return rbr;
    }
    /*
    @GetMapping("/allEmp")
    public List<Product> condition(){
        return productServiceimpl.selectAll();
    }


    @GetMapping("/condition")
    public ResponseEntity<BaseResult>  condition(ProductSearchVO productSearchVO){
        System.out.println(productSearchVO);
        ResponseEntity<BaseResult> rbr = productServiceimpl.selectAll(productSearchVO);
        return rbr;
    }
    @PostMapping("/addEmp")
    public BaseResult<List<Product>> addEmp(@RequestBody Product product){
        boolean add = productServiceimpl.add(product);
        System.out.println(add);
        if (add){
            return BaseResult.ok("新增成功！");
        }
        return BaseResult.error("新增失败！");
    }

    @GetMapping("/{id}")
    public BaseResult findById(@PathVariable("id") Integer id){
        Product product = productServiceimpl.findById(id);
        System.out.println(id);
        System.out.println(product);
        return BaseResult.ok("查询成功!", product);
    }

    @PutMapping
    public BaseResult updateByid(@RequestBody Product product){
        boolean flag = productServiceimpl.updateByid(product);
        System.out.println(flag);
        if (flag){
            return BaseResult.ok("修改成功！");
        }
        else {
            return BaseResult.error("修改失败！");
        }
    }

    @DeleteMapping("/{id}")
    public BaseResult deleteById(@PathVariable Integer id){
        boolean flag = productServiceimpl.del(id);
        if (flag){
            return BaseResult.ok("删除成功！");
        }
        else {
        return BaseResult.error("删除失败！");
        }
    }


    @DeleteMapping("/batchDelete/{ids}")
    public ResponseEntity<BaseResult> bachDelete(@PathVariable String ids){
        String[] splitIds = ids.split(",");
        for (String id:splitIds){
            productServiceimpl.deleteById(Integer.parseInt(id));
        }
        return BaseResult.success("删除成功");
    }
*/


    //TODO 登录用户
    @PostMapping("/login")
    public BaseResult<List<User>> login(@RequestBody User user){
        List<User> users = userServiceimpl.selectBykey(user);
        System.out.println(users);
        if (users!=null){
            for (User user1 : users) {
                String token = JWTUtil.createToken(1,user1.getUsername(),"user", 30);

                return BaseResult.ok(token);
            }
        }
        return BaseResult.error("用户名或密码错误！");
    }

    /**
     * 用户名校验
     * @param user
     * @return
     */
    @PostMapping("/checkname")
    public BaseResult checkname(@RequestBody User user) {
        try {
            System.out.println(user);
            // 注册
            User findUser = userServiceimpl.findByUsername(user);

            // 返回
            if(findUser == null ){
                System.out.println(findUser);
                return BaseResult.ok("用户名不正确");
            } else {
                return BaseResult.error("用户名正确");
            }
        } catch (Exception e) {
            return BaseResult.error(e.getMessage());
        }
    }

 /*   @PostMapping("/add")
    public BaseResult<List<Product>> add(@RequestBody Product product){
        int id = Integer.parseInt(JWTUtil.parseToken(httpServletRequest.getHeader("authorization"), "user").getId());
        boolean add = productServiceimpl.add(product);
        System.out.println(add);
        if (add){
            return BaseResult.ok("新增成功！");
        }
        return BaseResult.error("新增失败！");
    }
*/
}

package czxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class JwtAuth01Application {

    public static void main(String[] args) {
        SpringApplication.run(JwtAuth01Application.class, args);
    }

}

package czxy.web;

import czxy.common.BaseResult;
import czxy.service.impl.BrandServiceimpl;
import org.springframework.web.bind.annotation.*;
import pojo.Brand;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/brand")
public class GoodsController {
    @Resource
    private BrandServiceimpl brandServiceimpl;

    @GetMapping("/blist")
    public List<Brand> selectAllB(){
        List<Brand> brands = brandServiceimpl.selectAll();
        return brands;
    }



    @PostMapping("/add")
    public BaseResult<List<Brand>> add(@RequestBody Brand brand){
        boolean add = brandServiceimpl.add(brand);
        System.out.println(add);
        if (add){
            return BaseResult.ok("新增成功！");
        }
        return BaseResult.error("新增失败！");
    }




}

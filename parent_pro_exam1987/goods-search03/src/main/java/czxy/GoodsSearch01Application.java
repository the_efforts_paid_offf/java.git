package czxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoodsSearch01Application {

    public static void main(String[] args) {
        SpringApplication.run(GoodsSearch01Application.class, args);
    }

}

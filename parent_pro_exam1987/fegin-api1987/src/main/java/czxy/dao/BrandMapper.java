package czxy.dao;


import pojo.Brand;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ChinaManor
 * #Description BrandMapper
 * #Date: 19/11/2021 10:49
 */
@org.apache.ibatis.annotations.Mapper
public interface BrandMapper extends Mapper<Brand> {

}

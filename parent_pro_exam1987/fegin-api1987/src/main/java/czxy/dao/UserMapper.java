package czxy.dao;


import pojo.User;
import tk.mybatis.mapper.common.Mapper;


@org.apache.ibatis.annotations.Mapper

/**
 * @author ChinaManor
 * #Description UserMapper
 * #Date: 18/11/2021 16:35
 */

public interface UserMapper extends Mapper<User> {
}

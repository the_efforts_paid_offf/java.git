package czxy.dao;


import pojo.Product;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ChinaManor
 * #Description BrandMapper
 * #Date: 19/11/2021 10:49
 */
@org.apache.ibatis.annotations.Mapper
public interface ProductMapper extends Mapper<Product> {

}

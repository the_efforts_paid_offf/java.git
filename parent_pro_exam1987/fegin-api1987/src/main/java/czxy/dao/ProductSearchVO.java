package czxy.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data

public class ProductSearchVO {

    private String title;
    private Integer news_type_id;
    private String minpubDate;
    private String maxpubDate;
    private Integer page;//页码
    private Integer rows;//行数





}

package czxy.service;

import pojo.Brand;

import java.util.List;

/**
 * @author ChinaManor
 * #Description DeptServiceimpl
 * #Date: 19/11/2021 10:49
 */
public interface BrandService {
    public List<Brand> selectAll();

    boolean add(Brand brand);
}

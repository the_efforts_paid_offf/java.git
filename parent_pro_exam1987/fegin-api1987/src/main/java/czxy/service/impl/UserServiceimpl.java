package czxy.service.impl;

import czxy.dao.UserMapper;
import czxy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pojo.User;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
@Service
public class UserServiceimpl implements UserService {

    @Autowired
    UserMapper userMapper;


    @Override
    public List<User> selectBykey(User user) {
//        System.out.println(user);
        Example e = new Example(User.class);
        Example.Criteria c = e.createCriteria();
        if (user.getUsername()!=null&&!user.getUsername().equals("")){
            c.andEqualTo("username", user.getUsername());
        }
        if (user.getPassword()!=null&&!user.getPassword().equals("")){
            c.andEqualTo("password", user.getPassword());
        }
        List<User> users = userMapper.selectByExample(e);
        return users;
    }

    @Override
    public List<User> selectAll() {
        List<User> users = userMapper.selectAll();
        return users;
    }

    @Override
    public User findByUsername(User user) {
        Example example = new Example(User.class);
        Example.Criteria c = example.createCriteria();
        c.andEqualTo("username",user.getUsername());
        if (user.getUsername()!=null&&!user.getUsername().equals("")){
            c.andEqualTo("username", user.getUsername());
        }

        List<User> users = userMapper.selectByExample(example);
        for (User user1 : users) {
            return user1;
        }
        return null;
    }
}
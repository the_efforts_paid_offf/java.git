package czxy.service.impl;

import com.github.pagehelper.PageHelper;
import czxy.common.BaseResult;
import czxy.dao.BrandMapper;
import czxy.dao.ProductMapper;
import czxy.dao.CategoryMapper;
import czxy.service.ProductService;
import czxy.dao.ProductSearchVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pojo.Product;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @author ChinaManor
 * #Description EmpServiceimpl
 * #Date: 21/11/2021 15:40
 */

@Service
@Transactional
public class ProductServiceimpl implements ProductService {
    @Autowired
    ProductMapper productMapper;
    @Autowired
    BrandMapper brandMapper;
    @Autowired
    CategoryMapper categoryMapper;
//    @Autowired
//    ProductSearchVO productSearchVO;


/*    @Override
    public List<Product> selectAll(){
        List<Product> products = productMapper.selectAll();
        //后端关联id显示名字
        for (Product product : products) {
            product.setBrand(brandMapper.selectByPrimaryKey(product.getNews_type_id()));

        }
       return products;
    }*/
    @Override
    public ResponseEntity<BaseResult> selectAll(ProductSearchVO productSearchVO) {

        //拼接条件
        Integer page = productSearchVO.getPage();
        Example e = new Example(Product.class);
        Example.Criteria c = e.createCriteria();
        //1 名字模糊
        if (StringUtils.isNotBlank(productSearchVO.getTitle())){
            c.andLike("title","%"+productSearchVO.getTitle()+"%");
        }
        //2 学校id精确匹配

        if (StringUtils.isNotBlank(productSearchVO.getNews_type_id()+"")){
            c.andEqualTo("news_type_id",productSearchVO.getNews_type_id());
        }
        //3 >=最低薪资
        if (StringUtils.isNotBlank(productSearchVO.getMinpubDate()+"")){
            c.andGreaterThanOrEqualTo("pub_date",productSearchVO.getMinpubDate());
        }
        //4 <=最高薪资
        if (StringUtils.isNotBlank(productSearchVO.getMaxpubDate()+"")){
            c.andLessThanOrEqualTo("pub_date",productSearchVO.getMaxpubDate());
        }
        //查找总条数，供分页使用
        int total = productMapper.selectByExample(e).size();

        //7 后端分页
        PageHelper.startPage(productSearchVO.getPage(),productSearchVO.getRows());
        //8 查出结果
        List<Product> products = productMapper.selectByExample(e);


        //后端关联id显示名字
        for (Product product : products) {
            product.setBrand(brandMapper.selectByPrimaryKey(product.getNews_type_id()));
        }
        if (products.size()==0&&productSearchVO.getPage()!=1){
            page = total%productSearchVO.getRows()==0?total/productSearchVO.getRows():(total/productSearchVO.getRows()+1);
            productSearchVO.setPage(page);
            selectAll(productSearchVO);
        }

        return BaseResult.success(products,total);
    }

    @Override
    public boolean add(Product product) {

        return productMapper.insert(product)==1;
    }

    @Override
    public boolean del(Integer id) {
        return productMapper.deleteByPrimaryKey(id)==1;
    }

    @Override
    public boolean updateByid(Product product) {
        return productMapper.updateByPrimaryKey(product)==1;
    }

    @Override
    public boolean deleteById(int id) {
        return productMapper.deleteByPrimaryKey(id)==1;
    }

    @Override
    public Product findById(Integer id) {
        return productMapper.selectByPrimaryKey(id);
    }
}

package czxy.service.impl;

import czxy.dao.BrandMapper;
import czxy.dao.CategoryMapper;
import czxy.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pojo.Brand;
import pojo.Category;

import java.util.List;

/**
 * @author ChinaManor
 * #Description DeptServiceimpl
 * #Date: 19/11/2021 10:49
 */
@Service
public class BrandServiceimpl implements BrandService {
    @Autowired
    BrandMapper brandMapper;
    @Autowired
    CategoryMapper categoryMapper;
    @Override
    public List<Brand> selectAll() {
        List<Brand> brands = brandMapper.selectAll();
        return brands;
    }

    @Override
    public boolean add(Brand brand) {
        return brandMapper.insert(brand)==1;
    }
}

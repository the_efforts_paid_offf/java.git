package czxy.service;

import pojo.User;

import java.util.List;

/**
 * @author ChinaManor
 * #Description UserServiceimpl
 * #Date: 18/11/2021 16:34
 */
public interface UserService {
    List<User> selectBykey(User user);

    List<User> selectAll();

    User findByUsername(User user);
}

package czxy.service;

import czxy.common.BaseResult;
import czxy.dao.ProductSearchVO;
import org.springframework.http.ResponseEntity;
import pojo.Product;

import java.util.List;

/**
 * @author ChinaManor
 * #Description UserServiceimpl
 * #Date: 18/11/2021 16:34
 */
public interface ProductService {
//    List<Product> selectAll();
    ResponseEntity<BaseResult> selectAll(ProductSearchVO productSearchVO);
//     ResponseEntity<BaseResult> condition(ProductSearchVO productSearchVO);
    boolean add(Product product);

    Product findById(Integer id);

    boolean del(Integer id);

    boolean updateByid(Product product);


    boolean deleteById(int id);
}

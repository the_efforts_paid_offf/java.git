package com.nzqk.demo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.Table;


@Table(name = "category")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {

    /**
     *   `cid` INT(50) NOT NULL AUTO_INCREMENT COMMENT '分类id',
     *   `cname` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名字',
     *   `cdesc` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     */

    @Id
    private Integer cid;
    private String cname;
    private String cdesc;
}

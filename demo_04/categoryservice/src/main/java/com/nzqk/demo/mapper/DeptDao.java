package com.nzqk.demo.mapper;

import com.nzqk.demo.pojo.Dept;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Version 1.0
 * @Author:zhaoJiaCai
 * @Date:2021/11/12 星期五   20:01
 */
@org.apache.ibatis.annotations.Mapper
public interface DeptDao extends Mapper<Dept> {
}

package com.nzqk.demo.service.Impl;

import com.nzqk.demo.mapper.DeptDao;
import com.nzqk.demo.pojo.Dept;
import com.nzqk.demo.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Version 1.0
 * @Author:zhaoJiaCai
 * @Date:2021/11/12 星期五   20:02
 */
@Service
@Transactional
public class DeptImpl implements DeptService {

    @Autowired
    private DeptDao deptDao;

    @Override
    public List<Dept> test() {
        return deptDao.selectAll();
    }

    @Override
    public List<Dept> selectAll() {
        return deptDao.selectAll();
    }

    @Override
    public Dept query_one_Object(Integer id) {
        return deptDao.selectByPrimaryKey(id);
    }

    @Override
    public void update_one_Object(Dept dept) {
        deptDao.updateByPrimaryKeySelective(dept);
    }

    @Override
    public void add(Dept dept) {
        deptDao.insert(dept);
    }

    @Override
    public Integer delete(String id) {
        return deptDao.deleteByPrimaryKey(id);
    }
}

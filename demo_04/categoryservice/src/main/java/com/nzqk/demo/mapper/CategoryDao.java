package com.nzqk.demo.mapper;


import com.nzqk.demo.pojo.Category;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface CategoryDao extends Mapper<Category> {
}

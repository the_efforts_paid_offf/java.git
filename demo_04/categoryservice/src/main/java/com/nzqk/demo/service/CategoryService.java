package com.nzqk.demo.service;


import com.nzqk.demo.pojo.Category;

import java.util.List;


public interface CategoryService  {


    List<Category> test();


    List<Category> selectAll();


    Category query_one_Object(Integer id);


    void update_one_Object(Category category);


    void add(Category category);


    Integer delete(String id);



}

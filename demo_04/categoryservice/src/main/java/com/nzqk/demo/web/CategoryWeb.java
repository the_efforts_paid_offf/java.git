package com.nzqk.demo.web;


import com.nzqk.demo.pojo.Category;
import com.nzqk.demo.service.CategoryService;
import com.nzqk.demo.vo.BaseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/category")
@CrossOrigin
public class CategoryWeb {

    @Autowired
    private CategoryService categoryService;

    @GetMapping({"/test"})
    public BaseResult<List<Category>> test() {
        System.err.println("Category    ==  test");
        return BaseResult.ok("查询成功",categoryService.test());
    }

    @PostMapping({"/list"})
    public BaseResult<List<Category>> list() {
        return BaseResult.ok("查询成功",categoryService.test());
    }

    @GetMapping({"/query_one_Data/{id}"})
    public BaseResult query_one_Goods(@PathVariable("id") Integer id) {
        return BaseResult.ok("查询成功",categoryService.query_one_Object(id));
    }

    @PutMapping({"/update_one_Data"})
    public BaseResult update_one_Goods(@RequestBody Category category) {
        categoryService.update_one_Object(category);
        return BaseResult.ok("修改成功");
    }

    @PostMapping({"/add"})
    public BaseResult add(@RequestBody Category category) {
        System.err.println(category);
        categoryService.add(category);
        return BaseResult.ok("添加成功");
    }

    @DeleteMapping({"/delete/{id}"})
    public BaseResult delete(@PathVariable Integer id) {
        Integer delete = categoryService.delete(id+"");
        return BaseResult.ok("删除成功");
    }


}

package com.nzqk.demo.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "dept")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Dept {

    /**
     *   `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '部门id',
     *   `dept_name` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名字',
     *   `dept_content` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     */

    @Id
    private Integer id;
    private String deptName;
    private String deptContent;
}

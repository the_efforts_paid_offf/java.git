package com.nzqk.demo.web;

import com.nzqk.demo.pojo.Dept;
import com.nzqk.demo.service.DeptService;
import com.nzqk.demo.vo.BaseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Version 1.0
 * @Author:zhaoJiaCai
 * @Date:2021/11/12 星期五   20:06
 */
@RestController
@RequestMapping("/dept")
@CrossOrigin
public class DeptWeb {

    @Autowired
    private DeptService deptService;

    @GetMapping({"/test"})
    public BaseResult<List<Dept>> test() {
        System.err.println("Dept    ==  test");
        return BaseResult.ok("查询成功",deptService.test());
    }

    @PostMapping({"/list"})
    public BaseResult<List<Dept>> list() {
        return BaseResult.ok("查询成功",deptService.test());
    }

    @GetMapping({"/query_one_Data/{id}"})
    public BaseResult query_one_Goods(@PathVariable("id") Integer id) {
        return BaseResult.ok("查询成功",deptService.query_one_Object(id));
    }

    @PutMapping({"/update_one_Data"})
    public BaseResult update_one_Goods(@RequestBody Dept category) {
        deptService.update_one_Object(category);
        return BaseResult.ok("修改成功");
    }

    @PostMapping({"/add"})
    public BaseResult add(@RequestBody Dept category) {
        System.err.println(category);
        deptService.add(category);
        return BaseResult.ok("添加成功");
    }

    @DeleteMapping({"/delete/{id}"})
    public BaseResult delete(@PathVariable Integer id) {
        Integer delete = deptService.delete(id+"");
        return BaseResult.ok("删除成功");
    }


}

package com.nzqk.demo.service.Impl;


import com.nzqk.demo.mapper.CategoryDao;
import com.nzqk.demo.pojo.Category;
import com.nzqk.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class CategoryImpl implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;


    @Override
    public List<Category> test() {
        return categoryDao.selectAll();
    }

    @Override
    public List<Category> selectAll() {
        return categoryDao.selectAll();
    }

    @Override
    public Category query_one_Object(Integer id) {
        return categoryDao.selectByPrimaryKey(id);
    }

    @Override
    public void update_one_Object(Category category) {
        categoryDao.updateByPrimaryKeySelective(category);
    }

    @Override
    public void add(Category category) {
        categoryDao.insertSelective(category);
    }

    @Override
    public Integer delete(String id) {
        return categoryDao.deleteByPrimaryKey(id);
    }

}

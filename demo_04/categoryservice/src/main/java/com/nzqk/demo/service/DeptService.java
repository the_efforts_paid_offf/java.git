package com.nzqk.demo.service;

import com.nzqk.demo.pojo.Dept;

import java.util.List;

/**
 * @Version 1.0
 * @Author:zhaoJiaCai
 * @Date:2021/11/12 星期五   20:01
 */
public interface DeptService {

    List<Dept> test();


    List<Dept> selectAll();


    Dept query_one_Object(Integer id);


    void update_one_Object(Dept dept);


    void add(Dept dept);


    Integer delete(String id);
}

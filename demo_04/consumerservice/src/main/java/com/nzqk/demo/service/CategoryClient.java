package com.nzqk.demo.service;


import com.nzqk.demo.vo.BaseResult;
import com.nzqk.demo.pojo.Category;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient("12category-service")
public interface CategoryClient  {

    @GetMapping("/category/query_one_Data/{id}")
    BaseResult findById(@PathVariable("id") Integer id);

    @GetMapping({"/category/test"})
    BaseResult<List<Category>> findAll();

    @PostMapping({"/category/add"})
    BaseResult add(Category category);

    @PutMapping({"/category/update_one_Data"})
    BaseResult update(Category category);

    @DeleteMapping({"/category/delete/{id}"})
    BaseResult deleteById(@PathVariable Integer id);
}

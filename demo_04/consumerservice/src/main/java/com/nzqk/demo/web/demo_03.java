package com.nzqk.demo.web;

import com.alibaba.fastjson.JSONObject;
import com.nzqk.demo.pojo.Dept;
import com.nzqk.demo.service.DeptClient;
import com.nzqk.demo.vo.BaseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Version 1.0
 * @Author:zhaoJiaCai
 * @Date:2021/11/12 星期五   20:08
 */
@RestController
@RequestMapping("/demo_03")
@CrossOrigin
@RefreshScope//配置热更新
public class demo_03 {

    @Autowired
    private DeptClient deptClient;

    @GetMapping({"/{id}"})
    public Dept findById(@PathVariable Integer id) {
        BaseResult result = deptClient.findById(id);
        String json = JSONObject.toJSONString(result.getData());
        Dept dept = JSONObject.parseObject(json, Dept.class);
        System.out.println(dept);
        return dept;
    }

    @GetMapping({"/all"})
    public BaseResult<List<Dept>> findAll() {
        BaseResult<List<Dept>> all = deptClient.findAll();
        return all;
    }

    @PostMapping
    public BaseResult add(@RequestBody Dept dept) {
        BaseResult add = deptClient.add(dept);
        return add;
    }

    @PutMapping
    public BaseResult update(@RequestBody Dept dept) {
        System.out.println(dept);
        BaseResult update = deptClient.update(dept);
        return update;
    }

    @DeleteMapping({"/{id}"})
    public BaseResult deleteById(@PathVariable Integer id) {
        BaseResult baseResult = deptClient.deleteById(id);
        return baseResult;
    }
}

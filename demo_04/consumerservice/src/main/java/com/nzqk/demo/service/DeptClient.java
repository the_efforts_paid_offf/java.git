package com.nzqk.demo.service;

import com.nzqk.demo.pojo.Dept;
import com.nzqk.demo.vo.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Version 1.0
 * @Author:zhaoJiaCai
 * @Date:2021/11/12 星期五   20:08
 */
@FeignClient("category-service")
public interface DeptClient {

    @GetMapping("/dept/query_one_Data/{id}")
    BaseResult findById(@PathVariable("id") Integer id);

    @GetMapping({"/dept/test"})
    BaseResult<List<Dept>> findAll();

    @PostMapping({"/dept/add"})
    BaseResult add(Dept dept);

    @PutMapping({"/dept/update_one_Data"})
    BaseResult update(@RequestBody Dept dept);

    @DeleteMapping({"/dept/delete/{id}"})
    BaseResult deleteById(@PathVariable Integer id);
}

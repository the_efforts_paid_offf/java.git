package com.nzqk.demo.web;


import com.alibaba.fastjson.JSONObject;
import com.nzqk.demo.pojo.Category;
import com.nzqk.demo.service.CategoryClient;
import com.nzqk.demo.vo.BaseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/demo_02")
@CrossOrigin
@RefreshScope//配置热更新
public class demo_02 {

    @Autowired
    private CategoryClient categoryClient;


    @GetMapping({"/{id}"})
    public Category findById(@PathVariable Integer id) {
        BaseResult result = categoryClient.findById(id);
        String json = JSONObject.toJSONString(result.getData());
        Category category = JSONObject.parseObject(json, Category.class);
        System.out.println(category);
        return category;
    }


    @GetMapping({"/all"})
    public BaseResult<List<Category>> findAll() {
        BaseResult<List<Category>> all = categoryClient.findAll();
        return all;
    }


    @PostMapping
    public BaseResult add(@RequestBody Category category) {
        BaseResult add = categoryClient.add(category);
        return add;
    }


    @PutMapping
    public BaseResult update(@RequestBody Category category) {
        BaseResult update = categoryClient.update(category);
        return update;
    }


    @DeleteMapping({"/{id}"})
    public BaseResult deleteById(@PathVariable Integer id) {

        BaseResult baseResult = categoryClient.deleteById(id);
        return baseResult;
    }

}

package com.czxy.mybatis_0929_2.mapper;

import java.util.List;

/**
 * @author ChinaManor
 * #Description CategoryMapper
 * #Date: 30/9/2021 18:07
 */

import com.czxy.mybatis_0929_2.domain.Category;
import org.apache.ibatis.annotations.Param;

/**
 * @author 桐叔
 * @email liangtong@itcast.cn
 */
public interface CategoryMapper {

    /**
     * 查询指定父分类的所有分类
     * @param parentId
     * @return
     */
    public List<Category> selectAll(@Param("parentId") String parentId);
}
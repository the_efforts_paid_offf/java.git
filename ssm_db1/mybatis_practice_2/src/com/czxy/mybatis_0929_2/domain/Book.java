package com.czxy.mybatis_0929_2.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author ChinaManor
 * #Description Book
 * #Date: 30/9/2021 18:09
 */
@Entity(name="book")
public class Book {
    @Id
    private String bid;

    @Column(name = "title")
    private String title;

    @Column(name = "price")
    private Double price;

    @Column(name = "author")
    private String author;

    @Column(name = "cid")
    private String cid;

    //多对一：多本书 属于 一个分类
    private Category category;

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Book() {
    }

    public Book(String bid, String title, Double price, String author, String cid, Category category) {
        this.bid = bid;
        this.title = title;
        this.price = price;
        this.author = author;
        this.cid = cid;
        this.category = category;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bid='" + bid + '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", author='" + author + '\'' +
                ", cid='" + cid + '\'' +
                ", category=" + category +
                '}';
    }
}
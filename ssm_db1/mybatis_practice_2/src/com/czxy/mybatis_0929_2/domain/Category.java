package com.czxy.mybatis_0929_2.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ChinaManor
 * #Description Category
 * #Date: 30/9/2021 18:09
 */
    @Entity(name = "category")
    public class Category {
        @Id
        private String cid;

        @Column(name = "cname")
        private String cname;

        @Column(name="`desc`")
        private String desc;

        // 父分类id
        @Column(name="parent_id")
        private String parentId;

        //多对一：多个二级分类 对应 一个一级分类
        private Category category;

        //一对多：一个一级分类 对应 多个二级分类
        private List<Category> categoryList = new ArrayList<>();
    //一对多：一个二级分类 对应 多个书籍
    private List<Book> bookList = new ArrayList<>();

    @Override
    public String toString() {
        return "Category{" +
                "cid='" + cid + '\'' +
                ", cname='" + cname + '\'' +
                ", desc='" + desc + '\'' +
                ", parentId='" + parentId + '\'' +
                ", category=" + category +
                ", categoryList=" + categoryList +
                ", bookList=" + bookList +
                '}';
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    public Category() {
    }

    public Category(String cid, String cname, String desc, String parentId, Category category, List<Category> categoryList, List<Book> bookList) {
        this.cid = cid;
        this.cname = cname;
        this.desc = desc;
        this.parentId = parentId;
        this.category = category;
        this.categoryList = categoryList;
        this.bookList = bookList;
    }
}

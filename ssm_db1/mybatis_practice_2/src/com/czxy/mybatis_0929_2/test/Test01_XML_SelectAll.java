package com.czxy.mybatis_0929_2.test;


import com.czxy.mybatis_0929_2.domain.Book;
import com.czxy.mybatis_0929_2.utils.MyBatisUtils;
import com.czxy.mybatis_0929_2.domain.Category;
import com.czxy.mybatis_0929_2.mapper.CategoryMapper;
import org.junit.Test;

import java.util.List;

public class Test01_XML_SelectAll {
    public static void main(String[] args) {
        //1 获得mapper
        CategoryMapper categoryMapper = MyBatisUtils.getMapper(CategoryMapper.class);

        //2 查询所有的一级分类，同时查询二级分类，同时查询关联书籍
        List<Category> oneList = categoryMapper.selectAll("0");
        for (Category category : oneList) {
            System.out.println(category);
        }


        //3 释放
        MyBatisUtils.commitAndclose();
    }

    @Test
    public void Test1(){
        //1 获得mapper
        CategoryMapper categoryMapper = MyBatisUtils.getMapper(CategoryMapper.class);

        //2 查询所有的一级分类，同时查询二级分类，同时查询关联书籍
        List<Category> oneList = categoryMapper.selectAll("0");
        // 2.1 打印一级分类
        for (Category oneCategory : oneList) {
            System.out.println("one: " + oneCategory);
            // 2.2 打印二级分类
            for (Category twoCategory : oneCategory.getCategoryList()) {
                System.out.println("two: " + twoCategory);
            }
        }


        //3 释放
        MyBatisUtils.commitAndclose();
    }

    @Test
    public void test2(){
        //1 获得mapper
        CategoryMapper categoryMapper = MyBatisUtils.getMapper(CategoryMapper.class);

        //2 查询所有的一级分类，同时查询二级分类，同时查询关联书籍
        List<Category> oneList = categoryMapper.selectAll("0");
        // 2.1 打印一级分类
        for (Category oneCategory : oneList) {
            System.out.println("one: " + oneCategory);
            // 2.2 打印二级分类
            for (Category twoCategory : oneCategory.getCategoryList()) {
                System.out.println("two: " + twoCategory);
                // 2.3 书籍信息
                for (Book book : twoCategory.getBookList()) {
                    System.out.println(book);
                }
            }
        }


        //3 释放
        MyBatisUtils.commitAndclose();
    }
}
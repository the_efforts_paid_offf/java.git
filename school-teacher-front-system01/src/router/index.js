import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('@/views/About.vue')
  },
  {
    path: '/schoolList',
    name: 'SchoolList',
    component: () => import('@/views/SchoolList.vue')
  },
  {
    path: '/teacherList',
    name: 'TeacherList',
    component: () => import('@/views/TeacherList.vue')
  },
  {
    path: '/addTeacher',
    name: 'AddTeacher',
    component: () => import('@/views/AddTeacher.vue')
  },
  {
    path: '/updateTeacher/:id(\\d+)',
    name: 'UpdateTeacher',
    component: () => import('@/views/UpdateTeacher.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

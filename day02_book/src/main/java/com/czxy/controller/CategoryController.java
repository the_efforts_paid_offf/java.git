package com.czxy.controller;

import com.czxy.domain.Category;
import com.czxy.service.CategoryService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/school")
public class CategoryController {

    @Resource
    private CategoryService categoryService;

    /**
     * 查询所有
     *
     * @return
     */
    @GetMapping("/list")
    public ResponseEntity<List<Category>> findAll( ){
//        System.out.println(categoryService.selectAll());
        return ResponseEntity.ok(categoryService.selectAll());
    }
}

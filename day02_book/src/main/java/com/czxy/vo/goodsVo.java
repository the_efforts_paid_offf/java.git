package com.czxy.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class goodsVo {
    private String category_id;
    private String brand_id;
    private String name;
    private String startAge;
    private String endAge;

}

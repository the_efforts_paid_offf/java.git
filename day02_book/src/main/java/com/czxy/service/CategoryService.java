package com.czxy.service;

import com.czxy.domain.Category;

import java.util.List;

public interface CategoryService {
    /**
     * 查询所有
     *
     * @return
     */
    public List<Category> selectAll();


}

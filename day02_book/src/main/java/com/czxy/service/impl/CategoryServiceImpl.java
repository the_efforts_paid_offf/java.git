package com.czxy.service.impl;


import com.czxy.domain.Category;
import com.czxy.dao.CategoryMapper;
import com.czxy.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    @Resource
    private CategoryMapper categoryMapper;

    /**
     * 查询所有
     *
     * @return
     */

    @Override
    public List<Category> selectAll() {
        return categoryMapper.selectAll();
    }

}

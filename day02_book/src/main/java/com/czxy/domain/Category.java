package com.czxy.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


@Table(name = "t_school")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    //    id
//            name
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "school_name")
    private String school_name;

}

package com.nzqk.demo.service.Impl;


import com.nzqk.demo.dao.TypeDao;
import com.nzqk.demo.domin.Type;
import com.nzqk.demo.service.TypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;



@Service
@Transactional
public class TypeImpl implements TypeService {

    @Resource
    private TypeDao typeDao;


    @Override
    public List<Type> test() {
        return typeDao.selectAll();
    }

    @Override
    public List<Type> selectAll() {
        return typeDao.selectAll();
    }

    @Override
    public Type query_one_Object(Integer id) {
        return typeDao.selectByPrimaryKey(id);
    }

    @Override
    public void update_one_Object(Type dept) {
        typeDao.updateByPrimaryKeySelective(dept);
    }

    @Override
    public void add(Type dept) {
        typeDao.insert(dept);
    }

    @Override
    public Integer delete(String id) {
        return typeDao.deleteByPrimaryKey(id);
    }
}

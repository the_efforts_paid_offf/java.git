package com.nzqk.demo.dao;


import com.nzqk.demo.domin.Type;
import tk.mybatis.mapper.common.Mapper;


@org.apache.ibatis.annotations.Mapper
public interface TypeDao extends Mapper<Type> {
}

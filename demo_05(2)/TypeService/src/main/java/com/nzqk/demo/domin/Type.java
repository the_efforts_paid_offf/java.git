package com.nzqk.demo.domin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.Table;


@Table(name = "t_type")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Type {

    /**
     *   `id` INT(11) NOT NULL AUTO_INCREMENT,
     *   `city` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     */

    @Id
    private Integer id;
    private String city;
}

package com.nzqk.demo.service;


import com.nzqk.demo.domin.Type;

import java.util.List;


public interface TypeService {

    List<Type> test();


    List<Type> selectAll();


    Type query_one_Object(Integer id);


    void update_one_Object(Type dept);


    void add(Type dept);


    Integer delete(String id);
}

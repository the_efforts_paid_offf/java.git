package com.nzqk.demo.controller;


import com.nzqk.demo.domin.Type;
import com.nzqk.demo.service.TypeService;
import com.nzqk.demo.vo.BaseResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/type")
@CrossOrigin
public class TypeController {

    @Resource
    private TypeService typeService;

    @GetMapping({"/test"})
    public BaseResult<List<Type>> test() {
        return BaseResult.ok("查询成功",typeService.test());
    }

    @PostMapping({"/list"})
    public BaseResult<List<Type>> list() {
        return BaseResult.ok("查询成功",typeService.test());
    }

    @GetMapping({"/query_one_Data/{id}"})
    public BaseResult query_one_Data(@PathVariable("id") Integer id) {
        return BaseResult.ok("查询成功",typeService.query_one_Object(id));
    }

    @PutMapping({"/update_one_Data"})
    public BaseResult update_one_Data(@RequestBody Type type) {
        typeService.update_one_Object(type);
        return BaseResult.ok("修改成功");
    }

    @PostMapping({"/add"})
    public BaseResult add(@RequestBody Type type) {
        typeService.add(type);
        return BaseResult.ok("添加成功");
    }

    @DeleteMapping({"/delete/{id}"})
    public BaseResult delete(@PathVariable Integer id) {
        Integer delete = typeService.delete(id+"");
        return BaseResult.ok("删除成功");
    }
}

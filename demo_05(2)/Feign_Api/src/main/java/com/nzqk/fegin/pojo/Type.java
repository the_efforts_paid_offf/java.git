package com.nzqk.fegin.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
public class Type {

    /**
     *   `id` INT(11) NOT NULL AUTO_INCREMENT,
     *   `city` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     */


    private Integer id;
    private String city;
}

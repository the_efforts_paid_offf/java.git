package com.nzqk.fegin.clients;


import com.nzqk.fegin.pojo.BaseResult;
import com.nzqk.fegin.pojo.Type;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@FeignClient("type-service")
public interface TypeClient {

    @GetMapping("/type/query_one_Data/{id}")
    BaseResult findById(@PathVariable("id") Integer id);

    @GetMapping({"/type/test"})
    BaseResult<List<Type>> findAll();

    @PostMapping({"/type/add"})
    BaseResult add(Type dept);

    @PutMapping({"/type/update_one_Data"})
    BaseResult update(@RequestBody Type dept);

    @DeleteMapping({"/type/delete/{id}"})
    BaseResult deleteById(@PathVariable Integer id);
}

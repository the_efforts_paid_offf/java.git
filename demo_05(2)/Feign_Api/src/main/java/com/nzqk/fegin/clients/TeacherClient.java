package com.nzqk.fegin.clients;


import com.nzqk.fegin.pojo.BaseResult;
import com.nzqk.fegin.pojo.Teacher;
import com.nzqk.fegin.pojo.Teacher_Vo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@FeignClient("teacher-service")
public interface TeacherClient {

    @GetMapping("/teacher/query_one_Data/{id}")
    BaseResult findById(@PathVariable("id") Integer id);

    @GetMapping({"/teacher/test"})
    BaseResult<List<Teacher>> findAll();

    @PostMapping({"/teacher/list"})
    BaseResult<List<Teacher>> selectAll(@RequestBody Teacher_Vo teacherVo);

    @PostMapping({"/teacher/add"})
    BaseResult add(Teacher teacher);

    @PutMapping({"/teacher/update_one_Data"})
    BaseResult update(@RequestBody Teacher teacher);

    @DeleteMapping({"/teacher/delete/{id}"})
    BaseResult deleteById(@PathVariable Integer id);
}

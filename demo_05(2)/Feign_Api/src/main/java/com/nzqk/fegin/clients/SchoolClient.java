package com.nzqk.fegin.clients;


import com.nzqk.fegin.pojo.BaseResult;
import com.nzqk.fegin.pojo.School;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@FeignClient("school-service")
public interface SchoolClient {

    @GetMapping("/school/query_one_Data/{id}")
    BaseResult findById(@PathVariable("id") Integer id);

    @GetMapping({"/school/test"})
    BaseResult<List<School>> findAll();

    @PostMapping({"/school/add"})
    BaseResult add(School school);

    @PutMapping({"/school/update_one_Data"})
    BaseResult update(@RequestBody School school);

    @DeleteMapping({"/school/delete/{id}"})
    BaseResult deleteById(@PathVariable Integer id);
}

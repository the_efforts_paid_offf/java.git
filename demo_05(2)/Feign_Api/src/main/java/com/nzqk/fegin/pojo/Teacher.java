package com.nzqk.fegin.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;



@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {

    /**
     * `id` INT(11) NOT NULL AUTO_INCREMENT,
     * `teacher_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     * `pwd` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     * `sex` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     * `salary` DECIMAL(10, 2) NULL DEFAULT NULL,
     * `type_id` INT(11) NULL DEFAULT NULL,
     * `school_id` INT(11) NULL DEFAULT NULL,
     * `hire_date` DATE NULL DEFAULT NULL,
     * `remark` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     */


    private Integer id;
    private String teacherName;
    private String pwd;
    private String sex;
    private Double salary;
    private Integer typeId;
    private Integer schoolId;
    private Date hireDate;
    private String remark;

    private School school;
    private Type type;

}

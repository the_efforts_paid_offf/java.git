package com.nzqk.demo.controller;


import com.nzqk.demo.domin.School;
import com.nzqk.demo.service.SchoolService;
import com.nzqk.demo.vo.BaseResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/school")
@CrossOrigin
public class SchoolController {

    @Resource
    private SchoolService schoolService;

    @GetMapping({"/test"})
    public BaseResult<List<School>> test() {
        return BaseResult.ok("查询成功",schoolService.test());
    }

    @PostMapping({"/list"})
    public BaseResult<List<School>> list() {
        return BaseResult.ok("查询成功",schoolService.test());
    }

    @GetMapping({"/query_one_Data/{id}"})
    public BaseResult query_one_Data(@PathVariable("id") Integer id) {
        return BaseResult.ok("查询成功",schoolService.query_one_Object(id));
    }

    @PutMapping({"/update_one_Data"})
    public BaseResult update_one_Data(@RequestBody School school) {
        schoolService.update_one_Object(school);
        return BaseResult.ok("修改成功");
    }

    @PostMapping({"/add"})
    public BaseResult add(@RequestBody School school) {
        schoolService.add(school);
        return BaseResult.ok("添加成功");
    }

    @DeleteMapping({"/delete/{id}"})
    public BaseResult delete(@PathVariable Integer id) {
        Integer delete = schoolService.delete(id+"");
        return BaseResult.ok("删除成功");
    }
}

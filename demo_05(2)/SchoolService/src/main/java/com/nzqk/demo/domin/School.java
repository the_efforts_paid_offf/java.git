package com.nzqk.demo.domin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


@Table(name ="t_school")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class School {

    /**
     *   `id` INT(11) NOT NULL AUTO_INCREMENT,
     *   `school_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     */

    @Id
    private Integer id;
    @Column(name = "school_name")
    private String schoolName;


}

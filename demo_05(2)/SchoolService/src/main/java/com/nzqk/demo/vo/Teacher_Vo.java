package com.nzqk.demo.vo;


public class Teacher_Vo {
    private String id; //学校id
    private String teacherName; //老师姓名
    private String startSalary; //薪资
    private String endSalary; //薪资

    private String startHreDate; //时间
    private String endHireDate; //时间

    public Teacher_Vo() {
    }

    public Teacher_Vo(String id, String teacherName, String startSalary, String endSalary, String startHreDate, String endHireDate) {
        this.id = id;
        this.teacherName = teacherName;
        this.startSalary = startSalary;
        this.endSalary = endSalary;
        this.startHreDate = startHreDate;
        this.endHireDate = endHireDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getStartSalary() {
        return startSalary;
    }

    public void setStartSalary(String startSalary) {
        this.startSalary = startSalary;
    }

    public String getEndSalary() {
        return endSalary;
    }

    public void setEndSalary(String endSalary) {
        this.endSalary = endSalary;
    }

    public String getStartHreDate() {
        return startHreDate;
    }

    public void setStartHreDate(String startHreDate) {
        this.startHreDate = startHreDate;
    }

    public String getEndHireDate() {
        return endHireDate;
    }

    public void setEndHireDate(String endHireDate) {
        this.endHireDate = endHireDate;
    }

    @Override
    public String toString() {
        return "Teacher_Vo{" +
                "id='" + id + '\'' +
                ", teacherName='" + teacherName + '\'' +
                ", startSalary='" + startSalary + '\'' +
                ", endSalary='" + endSalary + '\'' +
                ", startHreDate='" + startHreDate + '\'' +
                ", endHireDate='" + endHireDate + '\'' +
                '}';
    }
}

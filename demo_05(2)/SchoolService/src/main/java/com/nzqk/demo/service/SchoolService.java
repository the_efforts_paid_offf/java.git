package com.nzqk.demo.service;


import com.nzqk.demo.domin.School;

import java.util.List;


public interface SchoolService {

    List<School> test();


    List<School> selectAll();


    School query_one_Object(Integer id);


    void update_one_Object(School dept);


    void add(School dept);


    Integer delete(String id);
}

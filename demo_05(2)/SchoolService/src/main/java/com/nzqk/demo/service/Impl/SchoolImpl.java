package com.nzqk.demo.service.Impl;


import com.nzqk.demo.dao.SchoolDao;
import com.nzqk.demo.domin.School;
import com.nzqk.demo.service.SchoolService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@Service
@Transactional
public class SchoolImpl implements SchoolService {

    @Resource
    private SchoolDao schoolDao;


    @Override
    public List<School> test() {
        return schoolDao.selectAll();
    }

    @Override
    public List<School> selectAll() {
        return schoolDao.selectAll();
    }

    @Override
    public School query_one_Object(Integer id) {
        return schoolDao.selectByPrimaryKey(id);
    }

    @Override
    public void update_one_Object(School dept) {
        schoolDao.updateByPrimaryKeySelective(dept);
    }

    @Override
    public void add(School dept) {
        schoolDao.insert(dept);
    }

    @Override
    public Integer delete(String id) {
        return schoolDao.deleteByPrimaryKey(id);
    }
}

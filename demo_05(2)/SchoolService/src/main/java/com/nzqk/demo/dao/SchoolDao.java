package com.nzqk.demo.dao;

import com.nzqk.demo.domin.School;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface SchoolDao extends Mapper<School> {
}

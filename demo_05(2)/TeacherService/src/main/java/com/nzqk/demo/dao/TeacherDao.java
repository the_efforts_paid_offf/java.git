package com.nzqk.demo.dao;

import com.nzqk.demo.domin.Teacher;
import tk.mybatis.mapper.common.Mapper;


@org.apache.ibatis.annotations.Mapper
public interface TeacherDao extends Mapper<Teacher> {
}

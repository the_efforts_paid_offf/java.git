package com.nzqk.demo.controller;


import com.nzqk.demo.domin.Teacher;
import com.nzqk.demo.service.TeacherService;
import com.nzqk.demo.vo.BaseResult;
import com.nzqk.demo.vo.Teacher_Vo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/teacher")
@CrossOrigin
public class TeacherController {

    @Resource
    private TeacherService teacherService;

    @GetMapping({"/test"})
    public BaseResult<List<Teacher>> test() {
        return BaseResult.ok("查询成功",teacherService.test());
    }

    @PostMapping({"/list"})
    public BaseResult<List<Teacher>> list(@RequestBody Teacher_Vo teacherVo) {
        System.out.println(teacherVo);
        return BaseResult.ok("查询成功",teacherService.selectAll(teacherVo));
    }

    @GetMapping({"/query_one_Data/{id}"})
    public BaseResult query_one_Data(@PathVariable("id") Integer id) {
        return BaseResult.ok("查询成功",teacherService.query_one_Object(id));
    }

    @PutMapping({"/update_one_Data"})
    public BaseResult update_one_Data(@RequestBody Teacher teacher) {
        teacherService.update_one_Object(teacher);
        return BaseResult.ok("修改成功");
    }

    @PostMapping({"/add"})
    public BaseResult add(@RequestBody Teacher teacher) {
        teacherService.add(teacher);
        return BaseResult.ok("添加成功");
    }

    @DeleteMapping({"/delete/{id}"})
    public BaseResult delete(@PathVariable Integer id) {
        Integer delete = teacherService.delete(id+"");
        return BaseResult.ok("删除成功");
    }
}

package com.nzqk.demo.service;


import com.nzqk.demo.domin.Teacher;
import com.nzqk.demo.vo.Teacher_Vo;

import java.util.List;


public interface TeacherService {

    List<Teacher> test();


    List<Teacher> selectAll();

    List<Teacher> selectAll(Teacher_Vo teacherVo);

    Teacher query_one_Object(Integer id);


    void update_one_Object(Teacher dept);


    void add(Teacher dept);


    Integer delete(String id);
}

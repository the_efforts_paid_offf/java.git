package com.nzqk.demo.service.Impl;


import com.nzqk.demo.dao.TeacherDao;
import com.nzqk.demo.domin.Teacher;
import com.nzqk.demo.service.TeacherService;
import com.nzqk.demo.vo.Teacher_Vo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;


@Service
@Transactional
public class TeacherImpl implements TeacherService {

    @Resource
    private TeacherDao teacherDao;


    @Override
    public List<Teacher> test() {
        return teacherDao.selectAll();
    }

    @Override
    public List<Teacher> selectAll() {
        return teacherDao.selectAll();
    }

    @Override
    public List<Teacher> selectAll(Teacher_Vo teacherVo) {
        System.out.println(teacherVo);
        Example example = new Example(Teacher.class);
        Example.Criteria criteria = example.createCriteria();
        if (!isEmpty(teacherVo.getTeacherName())) {
            criteria.andLike("teacherName", "%" + teacherVo.getTeacherName() + "%");
        }
        if (!isEmpty(teacherVo.getStartSalary())) {
            criteria.andGreaterThanOrEqualTo("salary", teacherVo.getStartSalary());
        }
        if (!isEmpty(teacherVo.getEndSalary())) {
            criteria.andLessThanOrEqualTo("salary", teacherVo.getEndSalary());
        }
        if (!isEmpty(teacherVo.getStartHreDate())) {
            criteria.andGreaterThanOrEqualTo("hireDate", teacherVo.getStartHreDate());
        }
        if (!isEmpty(teacherVo.getEndHireDate())) {
            criteria.andLessThanOrEqualTo("hireDate", teacherVo.getEndHireDate());
        }
        List<Teacher> teachers = teacherDao.selectByExample(example);
        return teachers;
    }

    @Override
    public Teacher query_one_Object(Integer id) {
        return teacherDao.selectByPrimaryKey(id);
    }

    @Override
    public void update_one_Object(Teacher dept) {
        teacherDao.updateByPrimaryKeySelective(dept);
    }

    @Override
    public void add(Teacher dept) {
        teacherDao.insert(dept);
    }

    @Override
    public Integer delete(String id) {
        return teacherDao.deleteByPrimaryKey(id);
    }

    //判断是否为空
    public boolean isEmpty(String str){
        return str == null || "".equals(str);
    }
}

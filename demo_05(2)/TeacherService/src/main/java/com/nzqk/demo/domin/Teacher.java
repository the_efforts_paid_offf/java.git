package com.nzqk.demo.domin;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;


@Table(name = "t_teacher")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {

    /**
     * `id` INT(11) NOT NULL AUTO_INCREMENT,
     * `teacher_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     * `pwd` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     * `sex` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     * `salary` DECIMAL(10, 2) NULL DEFAULT NULL,
     * `type_id` INT(11) NULL DEFAULT NULL,
     * `school_id` INT(11) NULL DEFAULT NULL,
     * `hire_date` DATE NULL DEFAULT NULL,
     * `remark` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
     */

    @Id
    private Integer id;
    @Column(name = "teacher_name")
    private String teacherName;
    private String pwd;
    private String sex;
    private Double salary;
    @Column(name = "type_id")
    private Integer typeId;
    @Column(name = "school_id")
    private Integer schoolId;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @Column(name = "hire_date")
    private Date hireDate;
    private String remark;

}

package com.nzqk.demo.web;


import com.alibaba.fastjson.JSONObject;
import com.nzqk.fegin.clients.SchoolClient;
import com.nzqk.fegin.clients.TeacherClient;
import com.nzqk.fegin.clients.TypeClient;
import com.nzqk.fegin.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/demo_03")
@CrossOrigin
@RefreshScope//配置热更新
public class demo_03 {

    @Autowired
    private TeacherClient teacherClient;

    @Autowired
    private SchoolClient schoolClient;

    @Autowired
    private TypeClient typeClient;

    @GetMapping({"/{id}"})
    public Teacher findById(@PathVariable Integer id) {
        BaseResult result = teacherClient.findById(id);
        String json = JSONObject.toJSONString(result.getData());
        Teacher dept = JSONObject.parseObject(json, Teacher.class);
        return dept;
    }

    @GetMapping({"/all"})
    public BaseResult<List<Teacher>> findAll() {
        BaseResult<List<Teacher>> result = teacherClient.findAll();
        for (Teacher teacher : result.getData()) {
            teacher.setType(JSONObject.parseObject(JSONObject.toJSONString(typeClient.findById(teacher.getTypeId()).getData()), Type.class));
            teacher.setSchool(JSONObject.parseObject(JSONObject.toJSONString(schoolClient.findById(teacher.getSchoolId()).getData()), School.class));
        }
        return result;
    }

    @PostMapping({"/all"})
    public BaseResult<List<Teacher>> selectAll(@RequestBody Teacher_Vo teacherVo) {
        BaseResult<List<Teacher>> result = teacherClient.selectAll(teacherVo);

        System.err.println(result.getData());
        for (Teacher teacher : result.getData()) {
            teacher.setType(JSONObject.parseObject(JSONObject.toJSONString(typeClient.findById(teacher.getTypeId()).getData()), Type.class));
            teacher.setSchool(JSONObject.parseObject(JSONObject.toJSONString(schoolClient.findById(teacher.getSchoolId()).getData()), School.class));
        }

        if (teacherVo.getId().toString() != null && !teacherVo.getId().toString().equals("")) {
            ArrayList<Teacher> teachers = new ArrayList<>();
            for (Teacher teacher : result.getData()) {
                if (teacher.getSchoolId().toString().equals(teacherVo.getId().toString())) {
                    teachers.add(teacher);
                }
            }
            result.setData(teachers);
        }
        return result;
    }

    @PostMapping
    public BaseResult add(@RequestBody Teacher dept) {
        BaseResult add = teacherClient.add(dept);
        return add;
    }

    @PutMapping
    public BaseResult update(@RequestBody Teacher dept) {
        System.out.println(dept);
        BaseResult update = teacherClient.update(dept);
        return update;
    }

    @DeleteMapping({"/{id}"})
    public BaseResult deleteById(@PathVariable Integer id) {
        BaseResult baseResult = teacherClient.deleteById(id);
        return baseResult;
    }
}

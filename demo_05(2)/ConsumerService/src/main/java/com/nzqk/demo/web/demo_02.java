package com.nzqk.demo.web;


import com.alibaba.fastjson.JSONObject;
import com.nzqk.fegin.clients.SchoolClient;
import com.nzqk.fegin.pojo.BaseResult;
import com.nzqk.fegin.pojo.School;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/demo_02")
@CrossOrigin
@RefreshScope//配置热更新
public class demo_02 {


    @Autowired
    private SchoolClient schoolCient;


    @GetMapping({"/{id}"})
    public School findById(@PathVariable Integer id) {
        BaseResult result = schoolCient.findById(id);
        String json = JSONObject.toJSONString(result.getData());
        School dept = JSONObject.parseObject(json, School.class);
        return dept;
    }

    @GetMapping({"/all"})
    public BaseResult<List<School>> findAll() {
        BaseResult<List<School>> result = schoolCient.findAll();
        return result;
    }


    @PostMapping
    public BaseResult add(@RequestBody School dept) {
        BaseResult add = schoolCient.add(dept);
        return add;
    }

    @PutMapping
    public BaseResult update(@RequestBody School dept) {
        System.out.println(dept);
        BaseResult update = schoolCient.update(dept);
        return update;
    }

    @DeleteMapping({"/{id}"})
    public BaseResult deleteById(@PathVariable Integer id) {
        BaseResult baseResult = schoolCient.deleteById(id);
        return baseResult;
    }
}

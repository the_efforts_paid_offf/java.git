public class StudentManager {

    public void add(Students s)

    {

        s.logIn();

    }

    public void delete(Students s)

    {

        s.clearOut();

    }

   

    public static void main(String[] args) {

        // TODO Auto-generated method stub

        StudentManager manager=new StudentManager();

        Students underGraduate=new UnderGraduate();

        Students graduate=new Graduate();

        manager.add(underGraduate);

        manager.delete(underGraduate);

        manager.add(graduate);

        manager.delete(graduate);

    }

   

 

}

abstract class Students

{

    public String id;//学号

    public String name;//姓名

    public String className;//班级状态

    public abstract void logIn();

    public abstract void clearOut();

}

class UnderGraduate extends Students

{
    @Override
    public void logIn()

    {

        System.out.println("专科生注册，注册成功！");

    }
    @Override
    public void clearOut()

    {

        System.out.println("专科生注销，注销成功！");

    }

}

class Graduate extends Students

{
    @Override
    public void logIn()

    {

        System.out.println("本科生注册，注册成功！");

    }
    @Override
    public void clearOut()

    {

        System.out.println("本科生注销，注销成功！");

    }

   

}
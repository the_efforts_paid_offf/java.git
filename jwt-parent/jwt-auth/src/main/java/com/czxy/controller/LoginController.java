package com.czxy.controller;

import com.czxy.config.BaseResult;
import com.czxy.config.JWTUtil;
import com.czxy.jwtpojo.pojo.Goods;
import com.czxy.jwtpojo.pojo.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * @author ChinaManor
 * #Description GoodsController
 * #Date: 18/11/2021 11:15
 */

@RestController
@RequestMapping("/auth")
public class LoginController {

    @GetMapping("/login")
    public BaseResult login(String username,String password){
        if (username.equals("admin")&&password.equals("123456")){
            User user = new User(13, "admin", "123456");
            String token = JWTUtil.createToken(user.getId(), user.getUsername(), 30);
            return BaseResult.success(token);
        }
        return BaseResult.fail();
    }
}

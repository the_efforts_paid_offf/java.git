package com.czxy.config;

/**
 * Created by yxq .
 */
public class PageResult extends BaseResult {
    private Long total;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}

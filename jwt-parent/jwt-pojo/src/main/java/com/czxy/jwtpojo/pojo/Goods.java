package com.czxy.jwtpojo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Goods {

    private Integer skuid;
    private String goodsName;
    private Double price;
    
}
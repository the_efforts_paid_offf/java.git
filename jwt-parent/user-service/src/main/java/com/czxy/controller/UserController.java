package com.czxy.controller;

import com.czxy.config.BaseResult;
import com.czxy.jwtpojo.pojo.Goods;
import com.czxy.pojo.User;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * @author ChinaManor
 * #Description GoodsController
 * #Date: 18/11/2021 11:15
 */

@RestController
@RequestMapping("/user")
public class UserController {

    @PostMapping
    public BaseResult addUser(@RequestBody User user){
        System.out.println("新增成功!"+user);
        return BaseResult.success(null);
    }
}

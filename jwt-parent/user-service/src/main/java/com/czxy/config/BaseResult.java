package com.czxy.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseResult {

    private Integer code;
    private String msg;
    private Object data;

    public static BaseResult success(Object data){
        return new BaseResult(0,"成功",data);
    }

    public static BaseResult fail(){
        return new BaseResult(1,"失败",null);
    }

}

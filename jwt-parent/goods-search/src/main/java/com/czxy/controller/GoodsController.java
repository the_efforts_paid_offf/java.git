package com.czxy.controller;

import com.czxy.config.BaseResult;
import com.czxy.jwtpojo.pojo.Goods;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * @author ChinaManor
 * #Description GoodsController
 * #Date: 18/11/2021 11:15
 */

@RestController
@RequestMapping("/search")
public class GoodsController {

    @GetMapping
    public BaseResult search(){
        ArrayList<Goods> list = new ArrayList<>();
        list.add(new Goods(1,"xxx",255.0));
        list.add(new Goods(2,"yyy",265.0));
        list.add(new Goods(3,"zzz",275.0));
        return BaseResult.success(list);
    }
}
